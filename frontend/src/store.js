import Vue from 'vue'
import Vuex from 'vuex'
import TokenService from '@/services/TokenService'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: null,
    tokenType: null,
    apiURL: 'http://dev.apis.portnick.com/api/rest'
  },
  mutations: {
    changeToken (state, token) {
      state.token = token
    },
    changeTokenType (state, tokenType) {
      state.tokenType = tokenType
    }
  },
  actions: {
    async getToken ({ commit }) {
      const response = await TokenService.getToken()
      const data = response.data
      if (data.success) {
        const token = data.data.access_token
        const tokenType = data.data.token_type
        commit('changeToken', token)
        commit('changeTokenType', tokenType)
      }
    }
  }
})
