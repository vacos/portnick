import axios from 'axios'

const apiURL = (process.env.NODE_ENV === 'production') ? `https://www.portnick.com/proxy` : `http://dev.portnick.com/proxy`

export default (Authorization) => {
  return axios.create({
    baseURL: apiURL,
    withCredentials: false,
    headers: {
      'Accept': 'application/json',
      'Authorization': Authorization
    }
  })
}
