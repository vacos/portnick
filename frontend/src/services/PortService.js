import Api from '@/services/Api'

export default {
  getPort (typeToken, Token) {
    return Api(typeToken + ' ' + Token).get('/getPorts')
  }
}
