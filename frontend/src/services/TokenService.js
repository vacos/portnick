import Api from '@/services/Api'

export default {
  getToken () {
    return Api('Basic UG9ydG5pY2tfY2xpOlBvcnRuaWNrX3NFYw==').post('/getToken')
  }
}
