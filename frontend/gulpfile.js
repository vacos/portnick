var gulp = require('gulp')
var concat = require('gulp-concat')
var concatCss = require('gulp-concat-css')
var minifyCSS = require('gulp-minify-css')
var rename = require('gulp-rename')
var pathJS = 'public/assets/js/'
var pathCss = 'public/assets/cs/'
var critical = require('critical').stream

gulp.task('javascript', function () {
  return gulp.src([pathJS + 'modernizr.js', pathJS + 'pace.min.js'])
    .pipe(concat('modernizr_pace.min.js'))
    .pipe(gulp.dest(pathJS))
})

gulp.task('main', function () {
  return gulp.src([pathJS + 'jquery-2.1.3.min.js', pathJS + 'plugins.js', pathJS + 'main.js'])
    .pipe(concat('plugins.min.js'))
    .pipe(gulp.dest(pathJS))
})

gulp.task('all', function () {
  return gulp.src([pathJS + 'modernizr_pace.min.js', pathJS + 'plugins.min.js'])
    .pipe(concat('all.min.js'))
    .pipe(gulp.dest(pathJS))
})

gulp.task('css', function () {
  return gulp.src([pathCss + 'base.css', pathCss + 'vendor.css', pathCss + 'main.css'])
    .pipe(concatCss('bundle.css'))
    .pipe(gulp.dest(pathCss))

    .pipe(minifyCSS())
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest(pathCss))
})

gulp.task('critical', function () {
  return gulp
    .src('dist/index.html')
    .pipe(critical({ base: 'dist/', inline: true, css: ['dist/assets/cs/bundle.min.css'] }))
    .pipe(gulp.dest('dist'))
})
