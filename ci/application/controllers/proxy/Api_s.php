<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(dirname(__FILE__).'/Main_n.php');

class Api_s extends Main_n
{
    function __construct()
    {
        parent::__construct();
    }

    function get_token()
    {
        $authen = $this->getAuth();

        $this->curlData('POST','/oauth2/token/client_credentials',$authen);
    }

    function get_ports()
    {
        $authen = $this->getAuth();

        $this->curlData('GET','/products/category/65/limit/10',$authen);
    }
    
}