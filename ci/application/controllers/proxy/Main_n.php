<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Main_n extends CI_Controller
{

    private $allowedHeaders = array("GET", "POST", "PUT", "DELETE");
    private $accessControlAllowHeaders = array("Content-Type", "Authorization", "X-Requested-With", "X-Oc-Merchant-Id",
        "X-Oc-Merchant-Language", "X-Oc-Currency", "X-Oc-Image-Dimension", "X-Oc-Store-Id", "X-Oc-Session", "X-Oc-Include-Meta");

    function __construct()
    {
        parent::__construct();
    }

    protected function getRequestHeaders() 
    {
        $headers = apache_request_headers();
        $headers += getallheaders();
        return $headers;
    }

    protected function getAuth(){
        $headers = $this->getRequestHeaders();

        $authen = '';
        if(isset($headers['Authorization'])){
            $authen = $headers['Authorization'];
        }elseif(isset($headers['authorization'])){
            $authen = $headers['authorization'];
        }

        return $authen;
    }

    protected function response($data = '', $http_code = 200)
    {
        header_remove();// remove all previous header
        set_status_header($http_code);
        
        header('Content-Type: application/json');
        header("Access-Control-Allow-Origin:*");
        header("Access-Control-Allow-Methods:" . implode(", ", $this->allowedHeaders));
        header("Access-Control-Allow-Headers:*". implode(", ", $this->accessControlAllowHeaders));
        header("Access-Control-Allow-Credentials: true");
        
        echo $data;
        exit;
    }

    protected function curlData($method = 'get',$url = '',$authorization = '')
    {
        $ch = curl_init();

        $basePath = (ENVIRONMENT == 'production') ? 'https://apis.portnick.com/api/rest' : 'http://dev.apis.portnick.com/api/rest';

        $urlCurl = $basePath.$url;

        $ch = curl_init($urlCurl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: '.$authorization
            )
        );

        $response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_errno($ch)) {
            $message['message'] = curl_error($ch);

            $this->response( json_encode($message) );

            curl_close($ch);
            
        }else{
            $this->response($response);

            curl_close($ch);
        }

        

        

        
    }

}

